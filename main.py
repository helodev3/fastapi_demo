# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.

from typing import Optional

from fastapi import FastAPI, Request
from fastapi.middleware.cors import CORSMiddleware
import json


from fastapi.responses import StreamingResponse, FileResponse
app = FastAPI()
origins = [
    '*',
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=['GET', 'POST', 'PUT', 'HEAD', 'OPTIONS'],
    allow_headers=['*'],
)


@app.get("/")
def read_root():
    # return FileResponse('index.html')
    return [
        {'nom': 'Hiba'},
        {'nom': 'Khadija'}
    ]


@app.post("/ismael")
async def read_root(request: Request):
    return [
        {'nom': 'Ismael'}
    ]


@app.post('/talk')
async def talk(request: Request):

    body = await request.body()
    r_data: dict = json.loads(body)
    age = r_data['age']
    return {"nouveau_age": age*2}


@app.get("/items/{item_id}")
def read_item(item_id: int):
    return {"item_id": item_id}


@app.get('/widget')
def get_test_js():
    return FileResponse('js/rsv1widget.js')


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('Welcome to our servers \n')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/

# TODO include audio inside the widget
# TODO document READMEs
